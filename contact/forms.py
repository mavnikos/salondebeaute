from django.utils.translation import ugettext_lazy as _
from django import forms


class ContactForm(forms.Form):
    name = forms.CharField(
        max_length=90,
        required=True,
        label=_("Name"),
        label_suffix="",
        widget=forms.TextInput(
            attrs={"placeholder": _("Name"), "id": "name"}
        ),
    )
    subject = forms.CharField(
        label="Subject",
        label_suffix="",
        widget=forms.TextInput(
            attrs={"placeholder": _("Subject"), "id": "subject"}
        ),
    )
    email = forms.EmailField(
        label="Email",
        label_suffix="",
        required=True,
        error_messages={"invalid": _("Invalid email")},
        widget=forms.EmailInput(
            attrs={"placeholder": _("E-mail Address"), "id": "email"}
        ),
    )
    message = forms.CharField(
        label=_("Message"),
        label_suffix="",
        required=True,
        widget=forms.Textarea(
            attrs={"placeholder": _("Your Message"), "id": "message", "rows": "5"}
        ),
    )
