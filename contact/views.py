from smtplib import SMTPException

from django.contrib import messages
from django.shortcuts import render, reverse
from django.http import HttpResponseRedirect
from django.core.mail import EmailMessage, BadHeaderError
from django.utils.translation import ugettext_lazy as _
from django.conf import settings

from jinja2.exceptions import TemplateNotFound

from .forms import ContactForm


def get_template(template_name="contact_email_body.txt"):
    template = None
    env = settings.GET_JINJA_ENV()
    try:
        template = env.get_template(template_name)
    except TemplateNotFound:
        pass
    return template


def email_body(data):
    template = get_template()
    context = {"data": data}
    body = template.render(**context)
    return body


def contact_index(request):
    contact_form = ContactForm()
    if request.method == "POST":
        post_data = request.POST
        contact_form = ContactForm(post_data)
        if contact_form.is_valid():
            body = email_body(post_data)
            try:
                email = EmailMessage(
                    subject=post_data.get("subject", "New contact message"),
                    body=body,
                    from_email=settings.DEFAULT_FROM_EMAIL,
                    to=[settings.DEFAULT_TO_EMAIL],
                    reply_to=[post_data.get("email")],
                )
                email.send(fail_silently=True)
            except (BadHeaderError, SMTPException):
                messages.error(
                    request,
                    _("Form error submission. Please try again!"),
                    extra_tags="form_error",
                )
            else:
                messages.success(
                    request,
                    _("Thank you for your message!"),
                    extra_tags="form_success",
                )
                return HttpResponseRedirect(reverse("contact"))
        else:
            messages.error(
                request,
                _("Form has error(s). Please try again!"),
                extra_tags="form_error",
            )
    return render(request, "contact.html", {
        "contact_form": contact_form,
        "meta_description": "Contact us via telephone or email.",
    })
