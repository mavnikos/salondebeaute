from modeltranslation.translator import translator, TranslationOptions

from .models import HomePageSlider


class HomePageSliderTranslate(TranslationOptions):
    fields = ('text', )
    required_languages = None


translator.register(HomePageSlider, HomePageSliderTranslate)
