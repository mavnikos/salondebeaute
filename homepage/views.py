from django.views.generic import ListView

from .models import HomePageSlider, HomePagePuzzlePhoto


class HomePageView(ListView):
    template_name = 'homepage/index.html'
    queryset = HomePageSlider.objects.filter(is_active=True)
    context_object_name = 'sliders'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['puzzle_photos'] = HomePagePuzzlePhoto.objects.filter(is_active=True)
        return context
