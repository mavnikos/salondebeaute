from django.db import models

from django.utils.translation import ugettext_lazy as _

from progressiveimagefield.fields import ProgressiveImageField


class HomePageSlider(models.Model):
    image = ProgressiveImageField(
        _("Homepage slider image"),
        upload_to="homepage/",
        help_text=_(
            "Select an image that will be presented only on homepage slider.<br>"
            "The <em>Show image on slider?</em> checkbox must be checked.<br>"
            "This image should be 1920x1080px."
        ),
    )

    text = models.CharField(
        _("Overlay text"),
        max_length=100,
        blank=True,
        null=True,
        help_text=_("Text that will appear over the image. Max characters: 100."),
    )

    is_active = models.BooleanField(
        _("Show image on slider?"),
        default=True,
        help_text=_(
            "Select this <strong>ONLY</strong> if you select an image for the slider."
        ),
    )

    order = models.SmallIntegerField(
        _("Order of appearance"),
        blank=True,
        null=True,
        help_text=_(
            "A number which represents the order of appearance "
            "of this image in the slider."
        ),
    )

    class Meta:
        verbose_name = _("homepage slider photo")
        verbose_name_plural = _("Slider photos")
        ordering = ["order"]

    def __str__(self):
        return _("Slider photo {}").format(self.order)

    def get_slider_url(self):
        return self.image.url


class HomePagePuzzlePhoto(models.Model):
    image = ProgressiveImageField(
        _("Homepage image (puzzle)"),
        upload_to="homepage/",
        help_text=_(
            "Select an image that will be presented only on home page below the "
            "slider.<br>The one that looks like a puzzle.<br>"
            "The <em>Show image on homepage?</em> checkbox must be checked.<br>"
            "This image should be 390x390px."
        ),
    )

    is_active = models.BooleanField(
        _("Show image on puzzle?"),
        default=True,
        help_text=_(
            "Select this <strong>ONLY</strong> if you select " "an image for puzzle."
        ),
    )

    order = models.SmallIntegerField(
        _("Order of appearance"),
        blank=True,
        null=True,
        help_text=_(
            "A number which represents the order of appearance of "
            "this image in the puzzle<br>"
            "(if 1 it will appear at top, 2 and 3 at bottom etc)."
        ),
    )

    class Meta:
        verbose_name = _("homepage puzzle photo")
        verbose_name_plural = _("Puzzle photos")
        ordering = ["order"]

    def __str__(self):
        return _("Puzzle photo {}").format(self.order)

    def get_puzzle_url(self):
        return self.image.url
