from django.contrib import admin
from django.utils.html import format_html, mark_safe
from django.utils.translation import ugettext_lazy as _

from modeltranslation.admin import TranslationAdmin

from .models import HomePageSlider, HomePagePuzzlePhoto


@admin.register(HomePageSlider)
class HomePageSliderAdmin(TranslationAdmin):
    ordering = ['order']
    fields = (
        ('is_active', 'order'),
        ('text_en', 'text_el'),
        ('img', 'image'),
    )
    list_display = ['img', 'text', 'order']
    readonly_fields = ['img']
    list_display_links = ['img', 'text']

    def img(self, obj):
        if obj.image:
            return format_html(
                '<img src={0} width=100px height=70px>', obj.get_slider_url()
            )
        return mark_safe(
            _('<span style="color:red;font-weight:bold;">No image</span>'))
    img.short_description = _('Preview')


@admin.register(HomePagePuzzlePhoto)
class HomePagePuzzlePhotoAdmin(admin.ModelAdmin):
    ordering = ['order']
    fields = (
        ('is_active', 'order'),
        ('img', 'image')
    )
    list_display = ['img', 'order']
    readonly_fields = ['img']
    list_display_links = ['img', 'order']

    def img(self, obj):
        if obj.image:
            return format_html(
                '<img src={0} width=100px height=100px>', obj.get_puzzle_url()
            )
        return mark_safe(
            _('<span style="color:red;font-weight:bold;">No image</span>'))
    img.short_description = _('Preview')
