from django.db import models
from django.utils.translation import ugettext_lazy as _


class Bridal(models.Model):
    # name = models.CharField(
    #     _('Bridal Name'),
    #     max_length=50,
    #     help_text=_('Max characters: 50.'),
    # )

    # categories = models.ManyToManyField(
    #     BridalCategory,
    #     related_name='bridals',
    #     help_text=_('Select one or more categories this bridal belongs to.'),
    # )

    # description = RichTextField(
    #     _('Bridal description'),
    #     blank=True, null=True,
    #     help_text=_('Enter a description of this bridal.'),
    # )

    image = models.ImageField(blank=True, null=True)

    # is_active = models.BooleanField(
    #     _('Is bridal active?'),
    #     default=True,
    #     help_text=_('If checked, bridal will be shown.'),
    # )
    #
    # created_at = models.DateTimeField(auto_now_add=True)
    # updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('bridal')
        verbose_name_plural = _('bridals')

    # def __str__(self):
    #     return self.name


# class BridalCategory(models.Model):
#     name = models.CharField(
#         _('Bridal Category'),
#         max_length=50, unique=True,
#         help_text=_('Enter a name for the category of a bridal. '
#                     'Max characters: 50.'),
#     )
#
#     slug = models.SlugField(unique=True, max_length=60)
#
#     order = models.SmallIntegerField(
#         _('Order of appearance'),
#         blank=True, null=True,
#         help_text=_('A number representing this category appearance order.'),
#     )
#
#     created_at = models.DateTimeField(auto_now_add=True)
#     updated_at = models.DateTimeField(auto_now=True)
#
#     class Meta:
#         verbose_name = _('bridal category')
#         verbose_name_plural = _('bridal categories')
#         ordering = ['order', 'name']
#
#     def __str__(self):
#         return self.name


# class BridalImageDeposit(models.Model):
    # bridal = models.ForeignKey(
    #     Bridal,
    #     models.CASCADE,
    #     verbose_name=_('Bridal'),
    #     related_name='images',
    # )
    #
    # large_img = ProgressiveImageField(
    #     _('Large image'),
    #     upload_to='bridal/',
    #     help_text=_("Select the bridal's large image. "
    #                 "Max valid dimensions: 1920x1080px."),
    # )
    #
    # order = models.SmallIntegerField(
    #     _('Order of appearance'),
    #     blank=True, null=True,
    #     help_text=_('A number which represents the order of appearance.'),
    # )
    #
    # class Meta:
    #     verbose_name = _("bridal's large image")
    #     verbose_name_plural = _("bridal's large images")
    #     ordering = ['order']
    #
    # def __str__(self):
    #     return str(self.bridal)
    #
    # def get_large_image_url(self):
    #     return self.large_img.url
