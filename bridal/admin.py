from django.contrib import admin
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _

from .models import Bridal


@admin.register(Bridal)
class BridalAdmin(admin.ModelAdmin):
    # inlines = [BridalImageDepositInline]
    list_display = ["show_img", "image"]
    readonly_fields = ["show_img"]
    list_display_links = ["show_img"]
    fields = (
        ("show_img", "image"),
    )

    def show_img(self, obj):
        return format_html(
            "<img src={0} width=100>", obj.image.url,
        )

    show_img.short_description = _("Preview")


# class BridalImageDepositInline(admin.StackedInline):
#     model = BridalImageDeposit
#     readonly_fields = ["img"]
#     min_num = 1
#     extra = 1
#     fields = (("bridal", "order"), ("img", "large_img"))
#
#     def img(self, obj):
#         if obj.large_img:
#             return format_html(
#                 "<img src={0} width=70px height=90px>",
#                 obj.get_large_image_url(),
#             )
#         return mark_safe(
#             _('<span style="color:red;font-weight:bold;">No image</span>')
#         )
#
#     img.short_description = _("Preview")


# @admin.register(BridalCategory)
# class BridalCategoryAdmin(TranslationAdmin):
#     list_display = ["name", "order"]
#     list_display_links = ["name", "order"]
#     prepopulated_fields = {"slug": ["name"]}
#     fields = ("order", ("name_en", "name_el"), "slug")
