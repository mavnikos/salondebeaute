from django.shortcuts import render

from .models import Bridal


def bridal_index(request):
    bridals = Bridal.objects.all()
    return render(request, "bridal.html", {
        "bridals": bridals,
    })
