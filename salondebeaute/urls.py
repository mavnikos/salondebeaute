from django.urls import path, re_path, include
from django.conf.urls.i18n import i18n_patterns
from django.contrib import admin
# from django.contrib.sitemaps.views import sitemap
from django.views.generic import TemplateView
from django.conf import settings

from homepage.views import HomePageView
from service.views import woman_index
from bridal.views import bridal_index
from contact.views import contact_index
from .sitemap import SITEMAPS


# i18n prefix URLs
urlpatterns = [
    path("i18n/", include("django.conf.urls.i18n")),
]

urlpatterns += i18n_patterns(
    path("%s/" % settings.MY_ADMIN_URL, admin.site.urls),
    path("", HomePageView.as_view(), name='homepage'),
    path("woman/<slug:category>/", woman_index, name="spa_category"),
    path("man/", TemplateView.as_view(template_name="man.html"), name="man"),
    # path("bridal/", bridal_index, name="bridal"),
    path("nails/", woman_index, {"category": "nails"},  name="nails"),
    path("massage/", TemplateView.as_view(template_name="massage.html"), name="massage"),
    path("treatments/", TemplateView.as_view(template_name="treatments.html"), name="treatments"),
    path("contact/", contact_index, name="contact"),
)

# Uncomment below to support sitemap
# urlpatterns += [
#    re_path(r'^sitemap\.xml/$',
#            sitemap,
#            {'sitemaps': SITEMAPS, 'template_name': 'sitemap.xml'},
#            name='django.contrib.sitemaps.views.sitemap'
#            )
# ]

# Uncomment below to serve uploaded files and custom error pages
if settings.DEBUG:
    from django.views.static import serve
    from django.views.defaults import page_not_found, server_error

    urlpatterns += [
        re_path(r"^media/(?P<path>.*)", serve, {"document_root": settings.MEDIA_ROOT})
    ]

    urlpatterns += [
        path("404/", page_not_found, {"exception": ""}),
        path("500/", server_error),
    ]
