from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.translation import override
from django.utils.text import slugify
from django.urls import reverse

from ckeditor.fields import RichTextField


class SpaServiceCategory(models.Model):
    name = models.CharField(
        _("Service name"),
        max_length=100,
        unique=True,
        help_text=_("Enter a service name. Max characters: 100."),
    )

    header = models.CharField(max_length=100, blank=True)

    slug = models.SlugField(unique=True, max_length=100)

    top_image = models.ImageField(
        "Top image",
        blank=True,
        null=True,
        upload_to="woman/",
        help_text=_("Woman top image. Valid dimensions: 1920x330px."),
    )

    center_image = models.ImageField(
        "Center image",
        blank=True,
        null=True,
        upload_to="woman/",
        help_text=_("Woman center image. Valid dimensions: 370x500px."),
    )

    bottom_image = models.ImageField(
        "Bottom image",
        blank=True,
        null=True,
        upload_to="woman/",
        help_text=_("Woman bottom image. Valid dimensions 1000x560."),
    )

    body = RichTextField()

    order = models.SmallIntegerField(
        _("Order of appearance"),
        blank=True,
        null=True,
        help_text=_("A number representing the order of appearance."),
    )

    meta_description = models.CharField(
        _("Meta description"), max_length=160, blank=True
    )

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _("service category")
        verbose_name_plural = _("service categories")
        ordering = ["order", "name"]

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        with override(language="en"):
            self.slug = slugify(self.name)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse("spa_category", kwargs={"category": self.slug})

    def get_top_image_url(self):
        return self.top_image.url

    def get_center_image_url(self):
        return self.center_image.url

    def get_bottom_image_url(self):
        return self.bottom_image.url


# class SpaService(models.Model):
#     name = models.CharField(
#         _("Service name"),
#         max_length=150,
#         unique=True,
#         help_text=_("Service name. Max characters: 150."),
#     )
#
#     slug = models.SlugField(unique=True, max_length=150)
#
#     image = ProgressiveImageField(
#         _("Image"),
#         upload_to="service/",
#         help_text=_("Image for this service. Dimensions: 370x500px."),
#     )
#
#     category = models.ForeignKey(
#         SpaServiceCategory,
#         models.CASCADE,
#         related_name="services",
#         help_text=_("Select a category to which this service belongs to."),
#     )
#
#     description = RichTextField(_("Description"), blank=True, null=True)
#
#     order = models.SmallIntegerField(
#         _("Order of appearance"),
#         blank=True,
#         null=True,
#         help_text=_(
#             "A number representing the order of appearance of this service."
#         ),
#     )
#
#     is_active = models.BooleanField(
#         _("Is this service active?"),
#         default=True,
#         help_text=_(
#             "If checked then this service will be visible "
#             "across the website. If not it will be hidden."
#         ),
#     )
#
#     created_at = models.DateTimeField(auto_now_add=True)
#     updated_at = models.DateTimeField(auto_now=True)
#
#     class Meta:
#         verbose_name = _("service")
#         verbose_name_plural = _("services")
#         ordering = ["order", "name"]
#
#     def __str__(self):
#         return self.name
#
#     def save(self, *args, **kwargs):
#         with override("en"):
#             self.slug = slugify(self.name)
#         super().save(*args, **kwargs)
#
#     def get_absolute_url(self):
#         return reverse("spa_service", kwargs={"service_slug": self.slug})
#
#     def get_image_url(self):
#         return self.image.url


# class ServiceImageDeposit(models.Model):
#     service = models.ForeignKey(
#         SpaService,
#         models.CASCADE,
#         verbose_name=_('Service'),
#         related_name='images',
#     )
#
#     large_img = models.ImageField(_('Large image'), upload_to='services/',
#                                     help_text=_('<strong>REQUIRED</strong>.<br>Select the service\'s large image. '
#                                                 'Max valid dimensions: 1920 x 1080px.'))
#     # ppoi_large = PPOIField('Large image PPOI')
#
#     # OPTIONAL FIELDS
#     order = models.SmallIntegerField(_('Order of appearance'), blank=True, null=True,
#                                      help_text=_('A number which represents the order of appearance of this image.'))
#
#     class Meta:
#         verbose_name = _('service\'s large image')
#         verbose_name_plural = _('service\'s large images')
#         ordering = ['order']
#
#     def __str__(self):
#         return self.service.name
#
#     def get_large_image_url(self):
#         return self.large_img.url

# duration = models.CharField(_('Duration'), max_length=50, blank=True, null=True,
#                             help_text=_('Enter the duration that this service lasts. '
#                                         'For example: 20 min or 1 hour and 5 min etc.'))

# is_featured = models.BooleanField(_('Is this service featured?'), default=False,
#                                   help_text=_('If checked, this service will be shown on the homepage as a featured'
#                                               ' service in the relevant section.'))


# NOT USED - MAYBE IN THE FUTURE...?
# class Person(models.Model):
#     full_name = models.CharField(
#         _('Full name'),
#         max_length=80,
#     )
#
#     selfie = models.ImageField(
#         _('Selfie'),
#         upload_to='homepage_images/',
#         help_text=_('Valid dimensions: 250x250px.'),
#     )
#
#     specialty = models.CharField(
#         _('Specialty'),
#         max_length=50,
#         blank=True, null=True,
#         help_text=_("Person\'s specialty, i.e: skin expert or massagist."),
#     )
#
#     email = models.EmailField(_("Person\'s email"), blank=True, null=True)
#
#     order = models.SmallIntegerField(
#         _('Order of appearance'),
#         blank=True, null=True,
#         help_text=_('Order of appearance of this person.'),
#     )
#
#     created_at = models.DateTimeField(auto_now_add=True)
#     updated_at = models.DateTimeField(auto_now=True)
#
#     class Meta:
#         verbose_name = _('person')
#         verbose_name_plural = _('Persons')
#         ordering = ['order', 'full_name']
#
#     def __str__(self):
#         return self.full_name
#
#     def get_selfie_url(self):
#         return self.selfie.url
