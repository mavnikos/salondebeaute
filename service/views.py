from django.shortcuts import render, get_object_or_404

from .models import SpaServiceCategory


def woman_index(request, category):
    spa_category = get_object_or_404(SpaServiceCategory, slug=category)
    return render(request, "woman.html", {"spa_category": spa_category})
