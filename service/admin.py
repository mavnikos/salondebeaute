from django.contrib import admin
from django.utils.html import format_html, mark_safe
from django.utils.translation import ugettext_lazy as _

from modeltranslation.admin import TranslationAdmin

from .models import SpaServiceCategory


@admin.register(SpaServiceCategory)
class SpaServiceCategoryAdmin(TranslationAdmin):
    fields = [
        ("name_en", "name_el", "order"),
        ("header_en", "header_el"),
        ("show_top_image", "top_image"),
        ("show_center_image", "center_image"),
        ("show_bottom_image", "bottom_image"),
        ("body_en", "body_el"),
        ("meta_description",)
    ]
    readonly_fields = ["show_top_image", "show_center_image", "show_bottom_image"]
    save_on_top = True

    def show_top_image(self, obj):
        if obj.top_image:
            return format_html(
                "<img src={0} width=100px>",
                obj.get_top_image_url(),
            )
        return mark_safe(
            _('<span style="color:red;font-weight:bold;">No image</span>')
        )

    def show_center_image(self, obj):
        if obj.center_image:
            return format_html(
                "<img src={0} width=100px>",
                obj.get_center_image_url(),
            )
        return mark_safe(
            _('<span style="color:red;font-weight:bold;">No image</span>')
        )

    def show_bottom_image(self, obj):
        if obj.bottom_image:
            return format_html(
                "<img src={0} width=100px>",
                obj.get_bottom_image_url(),
            )
        return mark_safe(
            _('<span style="color:red;font-weight:bold;">No image</span>')
        )


# class SpaServiceAdminInline(TranslationStackedInline):
#     model = SpaService
#     fields = [
#         ("name_en", "name_el"),
#         ("is_active", "order"),
#         "category",
#         ("description_en", "description_el"),
#         ("show_image", "image"),
#     ]
#     readonly_fields = ["show_image"]
#     min_num = 1
#     extra = 1
#
#     def show_image(self, obj):
#         if obj.image:
#             return format_html(
#                 "<img src={0} width=100px height=100px>", obj.get_image_url()
#             )
#         return mark_safe(
#             _('<span style="color:red;font-weight:bold;">No image</span>')
#         )
