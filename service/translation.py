from modeltranslation.translator import translator, TranslationOptions

from .models import SpaServiceCategory


class SpaServiceCategoryTranslate(TranslationOptions):
    fields = ("name", "header", "body", "meta_description")


translator.register(SpaServiceCategory, SpaServiceCategoryTranslate)
